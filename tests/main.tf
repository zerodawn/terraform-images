terraform {
  backend "http" {}
}

resource "local_file" "foo" {
  content  = "foo!"
  filename = "${path.module}/foo.bar"
}

resource "random_pet" "pet" {
  length = 2
}

output "pet" {
  value = random_pet.pet.id
}

variable "CI_PROJECT_NAME" {
  type    = string
  default = "default"
}

output "project_name" {
  value = var.CI_PROJECT_NAME
}
